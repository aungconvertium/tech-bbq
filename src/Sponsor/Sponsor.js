import React, { Component } from 'react';
import './Sponsor.css';


class Sponsor extends Component {
  render() {
    return (
      <section>
        <div className='sponsor'>
          <div>
            <h3>Premier Sponsor</h3>
            <img
              src="./images/cpl-logo.png"
              alt="Sponsor Logo"
              width='300px' />
            <img src="./images/cheers.svg" className="cheers-img" alt="Party Illustrator" />
          </div>
        </div>
        <Drinks />
      </section>
    );
  }
}

class Drinks extends Component {
  render() {
    let drinks = ['roku', 'carlsberg'];
    return(
      <div className='drinks'>
        <div>
          <h3>Official Drinks</h3>
          <ul className="list-unstyled">
            {drinks.map((drink, index) => (
              <li key={index} className={`drinks-${drink}`}>
                <img
                  src={`./images/${drink}-logo.png`}
                  alt={drink}
                  width='300px' />
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default Sponsor;