import React, { Component } from 'react';
import './Spotify.css';

class Agenda extends Component {
  render() {
    return (
      <section className='spotify padding'>
        <h2>Party Tunes</h2>
        <div className='spotify-playlist'>
          <iframe title="Spotify" src="https://open.spotify.com/embed/playlist/1LvSuXW5WSPgliJHgMzuWm"
            frameBorder="0" allowtransparency="true" allow="encrypted-media">
          </iframe>
        </div>
        <p>Feel free to add your favourite party songs.</p>
        <p>Without songs, a party is merely a group of people hanging around.</p>
        <p>And that's lame.</p>
      </section>
    );
  }
}

export default Agenda;
