import React, { Component } from 'react';
import './Upload.css';
import * as firebase from 'firebase';

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedFile: '', dbFiles: '' };
  }

  initialiseFile = (event) => {
    let file = event.target.files[0];
    this.setState({ selectedFile: file }, () => {
      console.log(this.state.selectedFile);
    });
  }

  uploadFile = () => {
    var fileName = (this.state.selectedFile).name;
    var storageRef = firebase.storage().ref('/images/' + fileName);
    var uploadTask = storageRef.put(this.state.selectedFile);

    // Register three observers:
    // 1. 'state_changed' observer, called any time the state changes
    // 2. Error observer, called on failure
    // 3. Completion observer, called on successful completion
    uploadTask.on('state_changed', function (snapshot) {
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
        default:
          break;
      }
    }, function (error) {
      // Handle unsuccessful uploads
    }, function () {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        console.log('File available at', downloadURL);
      });
    });
  }

  render() {
    return (
      <div className='upload-container'>
        <img src='https://static.thenounproject.com/png/42732-200.png' alt='work in progress' width='100px' />
        <h2>Work in progress, nothing to see here</h2>
        <label>
          Upload file
          <input type='file' className='file' onInput={this.initialiseFile}></input>
        </label>
        <button type="button" onClick={this.uploadFile}>Submit</button>
      </div>
    );
  }
}

export default Upload;