import React, { Component } from 'react';
import Tabletop from 'tabletop';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import './Setlist.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this);
    this.state = {
      collapse: false,
      data: [],
    };
  }

  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
    if (document.getElementById('setlist-expand').classList.contains('hidden')) {
      document.getElementById('setlist-expand').classList.remove('hidden');
      if (window.outerWidth > 768) {
        window.scroll({ top: 4000, behavior: 'smooth' });
      } else {
        window.scroll({ top: 3300, behavior: 'smooth' });
      }
    } else {
      document.getElementById('setlist-expand').classList.add('hidden');
      // window.scroll({ top: 4000, behavior: 'smooth' });
    }
    if (document.getElementById('setlist-texthide').classList.contains('hidden')
    ) {
      document.getElementById('setlist-texthide').classList.remove('hidden');
    } else {
      document.getElementById('setlist-texthide').classList.add('hidden');
    }
  }

  componentDidMount() {
    Tabletop.init({
      key: '1lkM2LOJ2AeDNkoOQHrnmwUcAQ_qstMwey6I_uYDS8dI',
      callback: googleData => {
        this.setState({ data: googleData });
      },
      simpleSheet: true
    })
  }

  render() {
    const data = this.state.data;
    return (
      <section className='setlist'>
        <div className='setlist-box'>
          <div className='setlist-smallbox'></div>
          <h2>Songs Setlist</h2>
          <p>Add the songs you want to be played in the GoogleSheet.</p>
          <hr />
          <Button onClick={this.toggle} style={{ marginBottom: '1rem' }}>
            <div className="btntextalign" id="setlist-texthide">Click for more
            <img className='expandBtn' id='setlist-expand' src='./images/expand.png' alt='expand button' />
            </div>
            <Collapse isOpen={this.state.collapse}>
              <Card>
                <CardBody>
                  {
                    data.map(obj => {
                      return (
                        <div key={obj.songTitle} className='song-div'>
                          <h3><b>{obj.songTitle}</b></h3>
                          <p>{obj.singer}</p>
                          <a href={obj.songLink} className='links' target='_blank' rel='noopener noreferrer'>Song</a>
                          <br />
                          <a href={obj.chordLink} className='links' target='_blank' rel='noopener noreferrer'>Chords</a>
                        </div>
                      )
                    })
                  }
                  <div className='setlist-closebtn'>Close</div>
                </CardBody>
              </Card>
            </Collapse>
          </Button>
        </div>
      </section>
    );
  }
}

export default App;
