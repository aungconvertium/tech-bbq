import React, { Component } from 'react';
import './Map.css';

class Map extends Component {
  render() {
    return (
      <section className='map-container'>
        <h2>Getting There</h2>
        <div className='map-canvas'>
          <iframe
            title='map'
            src='https://maps.google.com/maps?q=Civil%20Service%20Club%20%40%20Changi%2C%202%20Netheravon%20Rd%2C%20Singapore%20508503&t=&z=17&ie=UTF8&iwloc=&output=embed'
            width='750'
            height='500'
            frameBorder='0'
            scrolling='no'
            marginHeight='0'
            marginWidth='0'>
          </iframe>
        </div>
      </section>
    );
  }
}

export default Map;
