import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyBlxXe2Alt6Dm9r_xa7NK2dNZIdjlr_qFQ",
  authDomain: "tech-bbq.firebaseapp.com",
  databaseURL: "https://tech-bbq.firebaseio.com",
  storageBucket: 'gs://tech-bbq.appspot.com'
};
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
