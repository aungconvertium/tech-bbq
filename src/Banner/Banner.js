import React, { Component } from 'react';
import './Banner.css';
import Section from '../Section/Section';

class Banner extends Component {
  render() {
    const data = {
      title: 'Party!',
      sectionClass: 'banner',
      animationStyle: 'jump'
    };
    return (
      <div className='banner-container'>
        <img src="./images/party.jpg" alt='Party.jpg' />
        <div className='banner-text'>Are you ready to</div>

        <Section data={data} />
      </div>
    );
  }
}

export default Banner;