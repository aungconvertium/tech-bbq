import React, { Component } from 'react';
import './Banner2.css';

class Banner2 extends Component {
  render() {
    return (
      <div className="container">
        <span className="banner-title1">Let's Party!</span>
        <img src="./images/party.gif" className="partygif" alt="partygif"></img>
      </div>
    );
  }
}

export default Banner2;