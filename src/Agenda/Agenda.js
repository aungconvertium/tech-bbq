import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import './Agenda.css';

class Agenda extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false };
  }

  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
    if (document.getElementById('expand').classList.contains('hidden')
    ) {
      document.getElementById('expand').classList.remove('hidden');
      window.scroll({ top: 1500, behavior: 'smooth' });
    } else {
      document.getElementById('expand').classList.add('hidden');
      window.scroll({ top: 1700, behavior: 'smooth' });
    }

    if (document.getElementById('line').classList.contains('hidden')
    ) {
      document.getElementById('line').classList.remove('hidden');
    } else {
      document.getElementById('line').classList.add('hidden');
    }

    if (document.getElementById('texthide').classList.contains('hidden')
    ) {
      document.getElementById('texthide').classList.remove('hidden');

    } else {
      document.getElementById('texthide').classList.add('hidden');
    }

  }

  render() {
    const plans = [
      {
        time: '2:00 pm',
        topics: [
          'Go out from office'
        ]
      },
      {
        time: '3:00 pm',
        topics: [
          'Check in',
          'Opening speech'
        ]
      },
      {
        time: '3:15 pm',
        topics: [
          'Games',
          'Preparing for BBQ'
        ]
      },
      {
        time: '4:00 pm',
        topics: [
          'BBQ start',
          'Playing music',
          'Games',
          'Beer'
        ]
      },
      {
        time: '6:00 pm',
        topics: [
          'GIN',
          'Karaoke'
        ]
      },
      {
        time: '7:30 pm',
        topics: [
          'Go crazy...'
        ]
      },
      {
        time: '10:00 pm',
        topics: [
          'Out of control!'
        ]
      },
      {
        time: '10:00 am (next day)',
        topics: [
          'Check out'
        ]
      }
    ];

    return (
      <section className='agenda padding'>
        <p className='heading'>You are cordially invited to the esteemed Tech BBQ.</p>
        <p className='heading2'>Here are the events of the evening.</p>
        <div className='img-wrap'>
          <img src='./images/bbq-head.svg' alt='Svg head' width='700px' />
        </div>
        <div className='box border'>
          <div className='smallbox'></div>
          <h2>Agenda</h2>
          <ul className='list-inline'>
            <li><strong>Date:</strong> Sep 20, 2019</li>
            <li><strong>Time:</strong> 3:00pm onwards</li>
            <li><strong>Location:</strong> CSC at Changi</li>
          </ul>
          <hr />
          <Button onClick={this.toggle} style={{ marginBottom: '1rem' }}>
            {plans.slice(0, 2).map((plan, index) => (
              <div className='box event-agenda' key={index}>
                <div className='col'><h4>{plan.time}</h4></div>
                <div className='col event-details'>
                  {plan.topics.map((topic, i) => (
                    <p key={i}>{topic}</p>
                  ))}
                </div>
              </div>
            ))}
            <hr id="line" />
            <div className="btntextalign" id="texthide">Click for more
            <img className='expandBtn' id='expand' src='./images/expand.png' alt='expand button' />
            </div>
            <Collapse isOpen={this.state.collapse}>
              <Card>
                <CardBody>
                  {plans.slice(2, plans.length).map((plan, index) => (
                    <div className='box event-agenda' key={index}>
                      <div className='col'><h4>{plan.time}</h4></div>
                      <div className='col event-details'>
                        {plan.topics.map((topic, i) => (
                          <p key={i}>{topic}</p>
                        ))}
                      </div>
                    </div>
                  ))}
                  <div className='closebtn'>Close</div>
                </CardBody>
              </Card>
            </Collapse>
          </Button>
        </div>
        <div className='img-wrap'>
          <img src='./images/bbq-bottom.svg' alt='Svg bottom' width='700px' />
        </div>
      </section>
    );
  }
}

export default Agenda;
