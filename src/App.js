import React, { Component } from 'react';
import './App.css';

import Header from './Header/Header';
import Footer from './Footer/Footer';
import Banner from './Banner/Banner';
import Countdown from './Countdown/Countdown';
import Agenda from './Agenda/Agenda';
import Spotify from './Spotify/Spotify';
import Sponsor from './Sponsor/Sponsor';
// import Upload from './Upload/Upload';
import Map from './Map/Map';
import Setlist from './Setlist/Setlist';


const data = {
  header: {
    title: 'Tech BBQ'
  },
  footer: {
    copyright: 'Copyright &copy; 2019 Tech Team'
  }
};

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Header data={data.header} />
        <Banner />
        <Countdown />
        <Agenda />
        <Spotify />
        <Map />
        <Setlist />
        {/* <Upload /> */}
        <Sponsor />
        <Footer data={data.footer} />
      </div>
    );
  }
}

export default App;
