import React, { Component } from 'react';
import './Countdown.css';
import CountDown from 'react-countdown-now';

class Countdown extends Component {
  render() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    var date = '';
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      date = "2019-09-20T06:00:00".replace(/\s/, 'T');
    } else {
      date = "2019-09-20 14:00:00";
    }

    const Partystart = () => <span>Party Started!</span>;

    const renderer = ({days, hours, minutes, seconds, completed}) => {
      if (completed) {
        return <Partystart />;
      } else {
        return (
          <div className="countdown-timer">
            <div>
              <span>{days}</span>
              <span>days</span>
            </div>
            <div>
              <span>{hours}</span>
              <span>hours</span>
            </div>
            <div>
              <span>{minutes}</span>
              <span>minutes</span>
            </div>
            <div>
              <span>{seconds}</span>
              <span>seconds</span>
            </div>
          </div>
        );
      }
    };

    return (
      <div className='countdown'>
        <div>
          <h3>Hang tight, the party is soon:</h3>
          <CountDown
            date={date}
            renderer={renderer}
          />
        </div>
      </div>
    );
  }
}

export default Countdown;